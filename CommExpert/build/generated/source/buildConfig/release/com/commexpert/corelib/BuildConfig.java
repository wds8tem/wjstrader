/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.commexpert.corelib;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.commexpert.corelib";
  public static final String BUILD_TYPE = "release";
}
