package com.wjs.trader.ui.fragment

import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {

    protected fun setEditText(viewEdit: EditText?, isNormal: Boolean, isNumber: Boolean) {

        if(viewEdit!=null) {
            with(viewEdit)
            {
                isLongClickable = false
                isFocusableInTouchMode = true
                isFocusable = true
                ellipsize = null
                privateImeOptions = "defaultInputmode=english;"
                imeOptions = EditorInfo.IME_ACTION_DONE
                if (isNormal) {
                    inputType = if (isNumber)
                        InputType.TYPE_CLASS_NUMBER
                    else
                        InputType.TYPE_TEXT_VARIATION_URI or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                } else {
                    inputType = if (isNumber)
                        InputType.TYPE_CLASS_NUMBER or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    else
                        InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    viewEdit.transformationMethod = PasswordTransformationMethod.getInstance()
                }
            }
        }
    }
}