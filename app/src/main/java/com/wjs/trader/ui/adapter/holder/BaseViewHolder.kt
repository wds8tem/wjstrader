package com.wjs.trader.ui.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(
    val view : View
) : RecyclerView.ViewHolder(view) {

    open fun bind(item: Any?) {}
}