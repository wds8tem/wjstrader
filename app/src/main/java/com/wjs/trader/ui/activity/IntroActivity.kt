package com.wjs.trader.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.wjs.trader.R
import com.yuanta.smartnet.proc.SmartNetMng
import java.util.*

@SuppressLint("NewApi")
class IntroActivity : AppCompatActivity() {

    private val accountList: ArrayList<String> = ArrayList()
    private val accountNameList: ArrayList<String> = ArrayList()
    private val accountCodeList: ArrayList<String> = ArrayList()

    private val INTRO_DELAY = 3000L

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("wang", "start main activity")

        setContentView(R.layout.activity_intro)
        // 인트로를 보여주면서 퍼미션을 확인한다.
        showIntro()
    }

    override fun onDestroy() {
        super.onDestroy()

        accountList.clear()
        accountNameList.clear()
        accountCodeList.clear()

        SmartNetMng.releaseSmartNet()
    }

    private fun showIntro() {
        // 인트로를 보여준다.
        val handler = Handler()
        handler.postDelayed(Runnable {
            startActivity(Intent(IntroActivity@ this, MainActivity::class.java))
            //bottomNavigation.visibility = View.VISIBLE
        }, INTRO_DELAY)
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount > 3) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }
}
