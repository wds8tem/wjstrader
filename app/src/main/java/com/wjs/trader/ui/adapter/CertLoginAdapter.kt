package com.wjs.trader.ui.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.lumensoft.ks.KSCertificate
import com.wjs.trader.R
import com.wjs.trader.ui.adapter.holder.CertLoginHolder
import com.yuanta.smartnet.proc.SmartNetMng

class CertLoginAdapter(
        private val context: Context,
        certList: ArrayList<KSCertificate>
) :
        BaseAdapter<CertLoginHolder>() {
    private val items: ArrayList<KSCertificate> = ArrayList()

    init {
        this.items.clear()
        this.items.addAll(certList)
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CertLoginHolder =
            CertLoginHolder(
                    view = LayoutInflater.from(context)
                            .inflate(R.layout.list_item_certification, parent, false)
            )

    override fun onBindViewHolder(holder: CertLoginHolder, position: Int) {
        val cert = items[position]
        holder.apply {
            bind(cert)
            itemView.setOnClickListener {
                showPwdPopup(cert)
            }
        }
    }

    private fun showPwdPopup(cert: KSCertificate) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_cert_pwd, null)
        var etCertPwd: AppCompatEditText = view.findViewById(R.id.et_cert_pwd)
        val alertDialog = AlertDialog.Builder(context)
                .setTitle(R.string.cert_pwd)
                .setPositiveButton(R.string.next) { _, _ ->
                    // 일단 인증서 비밀번호만 확인을 한다.
                    // VM으로 가야되는 것 아닌가?
//                    if (isCertLogin) {
                    SmartNetMng.getInstance().startLogin(cert.subjectDn, etCertPwd.text.toString())
//                    } else if (isMotuLogin) {
//                        SmartNetMng.getInstance().startMotuLogin(userId, userPwd)
//                    } else {
//                        SmartNetMng.getInstance().startLogin(userId, userPwd, certPwd)
//
//                    }

                }
                .setNegativeButton(R.string.cancel, null)
                .create()
        alertDialog.setView(view)
        alertDialog.show()
    }
}