package com.wjs.trader.ui.adapter.holder

import android.view.View
import android.widget.TextView
import com.lumensoft.ks.KSCertificate
import com.wjs.trader.R

class CertLoginHolder(view: View) :
        BaseViewHolder(view) {

    private var tvCertSubjectDn: TextView? = null

    init {
        tvCertSubjectDn = itemView.findViewById(R.id.tv_cert_subject_dn)
    }

    override fun bind(cert: Any?) {
        if (cert is KSCertificate) {
            tvCertSubjectDn?.text = cert.subjectDn
        }
    }
}
