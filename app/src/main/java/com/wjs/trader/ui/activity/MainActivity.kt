package com.wjs.trader.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import com.wjs.trader.R
import com.wjs.trader.util.PermissionChecker
import com.yuanta.smartnet.intrf.ISmartNetInitListener
import com.yuanta.smartnet.intrf.ISmartNetLoginListener
import com.yuanta.smartnet.proc.SmartNetMng
import java.io.Serializable
import java.util.*

@SuppressLint("NewApi")
class MainActivity : AppCompatActivity(){

    private val PERMISSION = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_PHONE_STATE
    )

    private val accountList: ArrayList<String> = ArrayList()
    private val accountNameList: ArrayList<String> = ArrayList()
    private val accountCodeList: ArrayList<String> = ArrayList()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("wang", "start main activity")

        setContentView(R.layout.activity_main)
        // 인트로를 보여주면서 퍼미션을 확인한다.
        showIntro()

        val checker = PermissionChecker(this)
        //PermissionChecker.checkSelfPermission(this,PERMISSION.toString())
        if (checker.lacksPermissions(*PERMISSION)) {
            Toast.makeText(this, getString(R.string.msg_request_permission), Toast.LENGTH_SHORT)
                .show()
            requestPermissions()

        } else {
            SmartNetMng.initSmartNet(this)
        }
    }

    private fun requestPermissions() {

        val arrPermission = arrayOfNulls<String>(3)
        for (i in 0..2) {
            arrPermission[i] = PERMISSION[i]
        }

        this.requestPermissions(arrPermission, 1234)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1234 -> {

                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmartNetMng.initSmartNet(this)

//                    SmartNetMng.getInstance().setInitListener(this)
//                    SmartNetMng.getInstance().setLoginListener(this)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        accountList.clear()
        accountNameList.clear()
        accountCodeList.clear()

        SmartNetMng.releaseSmartNet()
    }



    private fun showIntro() {
        // 인트로를 보여준다.

//        val introFragment = IntroFragment()
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.base_fragment, introFragment)
//            .addToBackStack(null)
//            .commit()
    }

//    private fun showLoginView() {
//
//        val loginFragment = LoginFragment()
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.content, loginFragment, LOGIN_TAG)
//            .addToBackStack(null)
//            .commitAllowingStateLoss()
//    }

//    private fun showHomeView() {
//
//        val homeFragment = HomeFragment()
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.content, homeFragment, HOME_TAG)
//            .addToBackStack(null)
//            .commit()
//    }
//
//    fun showDataView(interSub: InterestSubject?) {
//
//        val dataFragment = DataFragment()
//        val bundle = Bundle()
//        bundle.putSerializable("item", interSub)
//        bundle.putSerializable("main", this)
//        dataFragment.arguments = bundle
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.content, dataFragment, DATA_TAG)
//            .addToBackStack(null)
//            .commit()
//    }

//    fun showMyReturnView(interSub: InterestSubject?) {
//
//        val myReturnFragment = MyReturnFragment()
//        val bundle = Bundle()
//        bundle.putSerializable("item", interSub)
//
//        myReturnFragment.arguments = bundle
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.content, myReturnFragment, MY_RETURN_TAG)
//            .addToBackStack(null)
//            .commit()
//    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount > 3) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }
}
