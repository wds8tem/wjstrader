package com.wjs.trader.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.wjs.trader.R
import com.wjs.trader.ui.view.DataView

class DataFragment : BaseFragment() {

    var dataView: DataView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onActivityCreated(savedInstanceState)

        val rootView: View = inflater.inflate(R.layout.fragment_data, container, false)
        //val item = arguments!!.getSerializable("item") as InterestSubject?

        dataView = DataView(requireActivity())
        dataView!!.initView(requireActivity())
        rootView.findViewById<LinearLayout>(R.id.data_content).addView(dataView)
        return rootView
    }

    override fun onDetach() {
        super.onDetach()
        dataView?.releaseView()
    }
}