package com.wjs.trader.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wjs.trader.R
import com.wjs.trader.ui.adapter.CertLoginAdapter
import com.yuanta.smartnet.intrf.ISmartNetInitListener
import com.yuanta.smartnet.intrf.ISmartNetLoginListener
import com.yuanta.smartnet.proc.SmartNetMng

class CertLoginFragment() : Fragment(), ISmartNetInitListener, ISmartNetLoginListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_cert_login, container, false)

        recyclerView = rootView.findViewById(R.id.rv_cert)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            // 인증서 정보를 가지고 와서 셋팅한다.
            val certList = SmartNetMng.getInstance().userCertList
            viewAdapter = CertLoginAdapter(context, certList)
            recyclerView.adapter = viewAdapter
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (isEnabled) {
                        isEnabled = false
                    }
                }
            })

        SmartNetMng.getInstance().setInitListener(this)
        SmartNetMng.getInstance().setLoginListener(this)

        return rootView
    }

    override fun onSessionConnecting() {
        Toast.makeText(
            requireContext(),
            getString(R.string.msg_session_connection),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onSessionConnected(isSuccess: Boolean, strErrorMsg: String) {
        Toast.makeText(requireContext(), "$isSuccess => $strErrorMsg", Toast.LENGTH_SHORT).show()
    }

    override fun onSessionReconnectFail(p0: String?, p1: String?) {
        Toast.makeText(requireContext(), "$p0 $p1", Toast.LENGTH_SHORT).show()
    }

    override fun onAppVersionState(isDone: Boolean) {
        Toast.makeText(
            requireContext(),
            getString(R.string.msg_library_version_check),
            Toast.LENGTH_SHORT
        )
            .show()
    }

    override fun onMasterDownState(isDone: Boolean) {
        Toast.makeText(
            requireContext(),
            getString(R.string.msg_master_file_download),
            Toast.LENGTH_SHORT
        )
            .show()
    }

    override fun onMasterLoadState(isDone: Boolean) {
        Toast.makeText(requireContext(), getString(R.string.msg_master_file_loading), Toast.LENGTH_SHORT).show()
    }

    override fun onInitFinished() {
        // 로그인 화면을 보여준다.
        //showLoginView()
    }

    override fun onRequiredRefresh() {
        Toast.makeText(
            requireContext(),
            getString(R.string.msg_session_reconnection),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onLoginStarted() {
        showProgress(getString(R.string.msg_on_login))
    }

    override fun onLoginResult(isSuccess: Boolean, strErrorMsg: String?) {

        hideProgress()

        // 로그인이 성공했다면
        if(isSuccess)
        {
            findNavController().navigate(R.id.action_CertLoginFragment_to_HomeFragment)
        }
    }

    private fun showProgress(strText: String) {

    }

    private fun hideProgress() {
    }
}
