package com.wjs.trader.ui.adapter

import android.os.Build
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH : RecyclerView.ViewHolder?> : RecyclerView.Adapter<VH>() {

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH
    abstract override fun getItemCount(): Int
    abstract override fun onBindViewHolder(holder: VH, position: Int)
    open fun saveConfigure() {}

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    protected fun bindNextFocus(
        curEditText: AppCompatEditText?, nextEditText: AppCompatEditText?
    ) {
        var maxLen = 0
        curEditText?.let {
            for (filter in it.filters) {
                if (filter is InputFilter.LengthFilter) {
                    maxLen = filter.max
                }
            }

            it.addTextChangedListener(object : TextWatcher {

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                    if (maxLen <= s.length) {
                        nextEditText?.requestFocus()
                    }
                }
            })
        }
    }
}