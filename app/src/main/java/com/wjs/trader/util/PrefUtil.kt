package com.wjs.trader.util

import android.content.Context

class PrefUtil {

    companion object {

        fun getPrefDataBool(context: Context, key: String, data: Boolean): Boolean =
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.getBoolean(key, data) ?: false

        fun setPrefDataBool(context: Context, key: String?, data: Boolean) {
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.let {
                val editor = it.edit()
                editor.putBoolean(key, data)
                editor.commit()
            }
        }

        fun getPrefDataLong(context: Context, key: String, data: Long): Long =
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.getLong(key, data) ?: -1L

        fun setPrefDataLong(context: Context, key: String, data: Long) {
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.let {
                val editor = it.edit()
                editor.putLong(key, data)
                editor.commit()
            }
        }

        fun getPrefString(context: Context, key: String, data: String?): String =
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.getString(key, data) ?: ""

        fun setPrefString(context: Context, key: String, data: String) {
            context.getSharedPreferences(
                context.packageName.replace(".", ""), Context.MODE_PRIVATE
            )?.let {
                val editor = it.edit()
                editor.putString(key, data)
                editor.commit()
            }
        }
    }
}